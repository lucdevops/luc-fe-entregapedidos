import { ClaseVehiculo } from './clase-vehiculo';
import {TipoVehiculo } from './tipo-vehiculo';

export class Vehiculo {
    placa: string;
    fecha_soat: Date;
    fecha_tecnicomecanica: Date;
    peso_carga: number;
    cubicaje: number;
    clase: ClaseVehiculo;
    tipo: TipoVehiculo;

    


    
    
}