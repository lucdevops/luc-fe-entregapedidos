import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Vehiculo } from '../model/vehiculo';
import { ClaseVehiculo } from '../model/clase-vehiculo';
import { TipoVehiculo } from '../model/tipo-vehiculo';
import { DatePipe} from '@angular/common';
 

@Injectable({
  providedIn: 'root'
})
export class PantallaVehiService {

  private crearWMS: string = 'http://192.168.1.79:8081/wms/crear';
  private datosWMS: string = 'http://192.168.1.79:8081/wms/datos';

  constructor(private http: HttpClient, private dp: DatePipe) { }

  cargarVehiculo(): Observable<Vehiculo[]> {
    return this.http.get<Vehiculo[]>(this.datosWMS + '/cargarVehiculo');
  }

  crearVehiculo(vehiculo) {
    return this.http.get(this.crearWMS + '/crearVehiculo/' + vehiculo.placa + '/' + vehiculo.fechaSoat + '/' + vehiculo.fechaTec + '/' + vehiculo.pesoCarga + '/' + vehiculo.cubicaje + '/' + vehiculo.tipoVehiculo + '/' + vehiculo.claseVehiculo);
  }

  getVehiculoPlaca(placa: string): Observable<any> {
    return this.http.get<Vehiculo>(this.datosWMS + "/" + placa);
  }

  getClaseVehiculo() {
    return this.http.get<ClaseVehiculo[]>(this.datosWMS + "/cargarClaseVehiculo");
  }

  getTipoVehiculo() {
    return this.http.get<TipoVehiculo[]>(this.datosWMS + "/cargarTipoVehiculo");
  }

  modificarVehiculo(vehiculo: Vehiculo) {
    console.log(vehiculo)
    return this.http.get(this.crearWMS + "/modificarVehiculo/" + vehiculo.placa + "/" + this.dp.transform(vehiculo.fecha_soat,'yyyy-MM-dd').toString() + "/" + this.dp.transform(vehiculo.fecha_tecnicomecanica,'yyyy-MM-dd').toString() + "/" + vehiculo.peso_carga + "/" + vehiculo.cubicaje+ "/" + vehiculo.clase.idClase);
  }

}
