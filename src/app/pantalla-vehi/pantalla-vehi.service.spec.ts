import { TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { PantallaVehiService } from './pantalla-vehi.service';

describe('PantallaVehiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PantallaVehiService = TestBed.get(PantallaVehiService);
    expect(service).toBeTruthy();
  });
});
