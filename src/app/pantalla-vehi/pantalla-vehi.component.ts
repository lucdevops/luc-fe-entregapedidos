import { Component, OnInit, Input } from '@angular/core';
import { Vehiculo } from '../model/vehiculo';
import { PantallaVehiService } from './pantalla-vehi.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ClaseVehiculo } from '../model/clase-vehiculo';
import { TipoVehiculo } from '../model/tipo-vehiculo';

@Component({
  selector: 'app-pantalla-vehi',
  templateUrl: './pantalla-vehi.component.html',
  styleUrls: ['./pantalla-vehi.component.css']
})

 
export class PantallaVehiComponent implements OnInit {

  getVehiculo: Vehiculo = new Vehiculo();
  vehiculo: Vehiculo[] = [];
  intForm: FormGroup;
  clase_vehiculo: ClaseVehiculo[] = [];
  tipo_vehiculo: TipoVehiculo[] = [];

  constructor(private vehiculoService: PantallaVehiService, private fb: FormBuilder, private router: Router) { }

  getVehiculoPlaca(vehiculo: Vehiculo): void {
    console.log(vehiculo);
    this.getVehiculo = vehiculo;
  }

  updateVehiculo() {
    this.vehiculoService.modificarVehiculo(this.getVehiculo)
      .subscribe(b => {
        console.log(b);
        window.location.href = '/pantalla';

      })
  }
 
  addVehiculo() {
    this.vehiculoService.crearVehiculo(this.intForm.value).subscribe(a => {
      console.log(a);
      window.location.href = '/pantalla';
    });
  }

  ngOnInit() {
    this.vehiculoService.getClaseVehiculo().subscribe(cv => {
      this.clase_vehiculo = cv;
    })
    this.vehiculoService.getTipoVehiculo().subscribe(tv => {
      this.tipo_vehiculo = tv;
    })

    this.intForm = this.fb.group({
      placa: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      fechaSoat: ['', Validators.required],
      fechaTec: ['', Validators.required],
      pesoCarga: ['', Validators.required],
      cubicaje: ['', Validators.required],
      claseVehiculo: ['', Validators.required],
      tipoVehiculo: ['', Validators.required]
    });
    this.vehiculoService.cargarVehiculo().subscribe(v => {
      this.vehiculo = v;
    })
  }

  guardarForm() {
    if (this.intForm.valid) {
      this.vehiculoService.crearVehiculo(this.intForm.value);
    }
  }

  get placa() { return this.intForm.get('placa'); }
  get fechasoat() { return this.intForm.get('fechaSoat'); }
  get fechatec() { return this.intForm.get('fechaTec'); }
  get peso() { return this.intForm.get('pesoCarga'); }
  get cubicaje() { return this.intForm.get('cubicaje'); }
  get claseVehiculo() { return this.intForm.get('claseVehiculo'); }
  get tipoVehiculo() { return this.intForm.get('tipoVehiculo'); }

}
