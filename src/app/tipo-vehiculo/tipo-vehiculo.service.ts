import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { TipoVehiculo} from '../model/tipo-vehiculo';
import { Vehiculo } from '../model/vehiculo';

@Injectable({
  providedIn: 'root'
})
export class TipoVehiculoService {

  private crearWMS: string = 'http://192.168.1.79:8081/wms/crear';
  private datosWMS: string = 'http://192.168.1.79:8081/wms/datos';

  constructor(private http: HttpClient) { }

  cargarTipoVehiculo(): Observable<TipoVehiculo[]> {
    return this.http.get<TipoVehiculo[]>(this.datosWMS + '/cargarTipoVehiculo');
  }

  
  crearTipoVehiculo(tipovehiculo:TipoVehiculo) {
    return this.http.get(this.crearWMS + '/crearTipoVehiculo/' + tipovehiculo.nombreVehiculo);
  }

  getTipoId(idVehiculo:number): Observable<any>{
    return this.http.get<TipoVehiculo>(this.datosWMS + "/" + idVehiculo);
  }
  
  modificarTipoVehiculo(tipovehiculo: TipoVehiculo){
    return this.http.get<TipoVehiculo>(this.crearWMS + "/modificarTipoVehiculo/" + tipovehiculo.idVehiculo + "/" + tipovehiculo.nombreVehiculo);
  }

}
