import { Component, OnInit, Input } from '@angular/core';
import { TipoVehiculoService} from './tipo-vehiculo.service';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { TipoVehiculo} from '../model/tipo-vehiculo';

@Component({
  selector: 'app-tipo-vehiculo',
  templateUrl: './tipo-vehiculo.component.html',
  styleUrls:['./tipo-vehiculo.component.css']
})
export class TipoVehiculoComponent implements OnInit {

  tipoForm: FormGroup;
  tipovehiculo: TipoVehiculo[]=[];
  myControl = new FormControl;
  getTipoVehiculo: TipoVehiculo = new TipoVehiculo();

   constructor(private tipovehiculoService: TipoVehiculoService, private fb: FormBuilder) { }

  
   getTipoId(tipovehiculo: TipoVehiculo): void {
    console.log(tipovehiculo);
    this.getTipoVehiculo = tipovehiculo;

  }
  guardarTipo() {
    this.tipovehiculoService.crearTipoVehiculo(
      this.tipoForm.value).subscribe(d => {
        console.log(d);
        window.location.href = '/tipovehiculo';
      });
  }

  editarTipo() {
    this.tipovehiculoService.modificarTipoVehiculo(this.getTipoVehiculo)
      .subscribe(b => {
        console.log(b);
        window.location.href = '/tipovehiculo';

      })
  }


  vhForm() {
    if (this.tipoForm.valid) {
      this.tipovehiculoService.crearTipoVehiculo(this.tipoForm.value);
    }
    console.log(this.tipoForm.value);
  }

  ngOnInit() {
    this.tipoForm = this.fb.group({
      nombreVehiculo: ['', Validators.required]
    });
    this.tipovehiculoService.cargarTipoVehiculo().subscribe(tv => {
      this.tipovehiculo = tv;
    })
  
  }

  get nombre() { return this.tipoForm.get('nombreVehiculo'); }


}
