import { Component, OnInit, Input } from '@angular/core';
import { ClaseVehiculoService } from './clase-vehiculo.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ClaseVehiculo } from '../model/clase-vehiculo';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { Vehiculo } from '../model/vehiculo';


@Component({
  selector: 'app-clase-vehiculo',
  templateUrl: './clase-vehiculo.component.html',
  styleUrls: ['./clase-vehiculo.component.css']
})
export class ClaseVehiculoComponent implements OnInit {

  clasevehiculo: ClaseVehiculo[] = [];
  claseForm: FormGroup;
  value = '';
  getClaseVehiculo: ClaseVehiculo = new ClaseVehiculo();



  constructor(private clasevehiculoService: ClaseVehiculoService, private fb: FormBuilder) { }

  filterPost = '';

  guardarClasev() {
    this.clasevehiculoService.crearClaseVehiculo(
      this.claseForm.value).subscribe(a => {
        console.log(a);
        window.location.href = '/clasevehiculo';
      });
  }
  
  editarClase() {
    this.clasevehiculoService.modificarVehiculo(this.getClaseVehiculo)
      .subscribe(b => {
        console.log(b);
        window.location.href = '/clasevehiculo';

      })
  }
 
  getClaseVehiculoId(clasevehiculo: ClaseVehiculo): void {
    console.log(clasevehiculo);
    this.getClaseVehiculo = clasevehiculo;

  }
  
  guardarForm() {
    if (this.claseForm.valid) {
      this.clasevehiculoService.crearClaseVehiculo(this.claseForm.value);
    }
    console.log(this.claseForm.value);
  }

  ngOnInit() {
    this.claseForm = this.fb.group({
      nombreClaseV: ['', Validators.required]
    });
    this.clasevehiculoService.cargarClaseVehiculo().subscribe(cv => {
      this.clasevehiculo = cv;
    }) 
    

  }
 
  

  get nombre() { return this.claseForm.get('nombreClaseV'); }


}
