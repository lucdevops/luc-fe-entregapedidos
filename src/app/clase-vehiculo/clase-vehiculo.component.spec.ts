import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaseVehiculoComponent } from './clase-vehiculo.component';

describe('ClaseVehiculoComponent', () => {
  let component: ClaseVehiculoComponent;
  let fixture: ComponentFixture<ClaseVehiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaseVehiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaseVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
