import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ClaseVehiculo } from '../model/clase-vehiculo';

@Injectable({
  providedIn: 'root'
})
export class ClaseVehiculoService {

  private crearWMS: string = 'http://192.168.1.79:8081/wms/crear';
  private datosWMS: string = 'http://192.168.1.79:8081/wms/datos';

  constructor(private http: HttpClient) { }

  cargarClaseVehiculo(): Observable<ClaseVehiculo[]> {
    return this.http.get<ClaseVehiculo[]>(this.datosWMS + '/cargarClaseVehiculo');
  }

  crearClaseVehiculo(clasevehiculo) {
    return this.http.get(this.crearWMS + '/crearClaseVehiculo/' + clasevehiculo.nombreClaseV);
  }

  getClaseId(idClase: number): Observable<any> {
    return this.http.get<ClaseVehiculo>(this.datosWMS + "/" + idClase);
  }

  modificarVehiculo(clasevehiculo: ClaseVehiculo) {
    console.log(clasevehiculo)
    return this.http.get(this.crearWMS + "/modificarClaseVehiculo/" + clasevehiculo.nombreClaseV);
  }


}
