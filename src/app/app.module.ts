import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DemoMaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule, Routes } from '@angular/router';
import { PantallaVehiComponent } from './pantalla-vehi/pantalla-vehi.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule } from '@angular/common/http';
import { ClaseVehiculoComponent } from './clase-vehiculo/clase-vehiculo.component';
import { TipoVehiculoComponent } from './tipo-vehiculo/tipo-vehiculo.component';
import { FilterPipe } from './pipes/filter.pipe';
import { DatePipe} from '@angular/common';


const routes: Routes = [
  { path: 'pantalla', component: PantallaVehiComponent },
  { path: 'clasevehiculo', component: ClaseVehiculoComponent },
  { path: 'tipovehiculo', component: TipoVehiculoComponent },
 

];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    PantallaVehiComponent,
    ClaseVehiculoComponent,
    TipoVehiculoComponent,
    FilterPipe,
   

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
